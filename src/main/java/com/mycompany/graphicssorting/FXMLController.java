package com.mycompany.graphicssorting;



import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FXMLController implements Initializable {
    
    @FXML
    private Label label;
   
    
    @FXML 
    public void bubblesort(ActionEvent event){
       
         BubbleSort x;
        x = new BubbleSort();
        x.setTitle("Bubble sort");
        x.setSize(500, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        
    }
    @FXML 
    public void selectionSort(ActionEvent event){
       
         Selection x;
        x = new Selection();
        x.setTitle("Selection sort");
        x.setSize(500, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        
    }
    
    @FXML 
    public void insertionSort(ActionEvent event){
       
         Insertion x;
        x = new Insertion();
        x.setTitle("Insertion sort");
        x.setSize(500, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        
    }
    
    @FXML 
    public void mergeSort(ActionEvent event){
       
         Merge x;
        x = new Merge();
        x.setTitle("Merge sort");
        x.setSize(500, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        
    }
    @FXML 
    public void quickSort(ActionEvent event){
       
         Quick x;
        x = new Quick();
        x.setTitle("Quick sort");
        x.setSize(500, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        
    }
    @FXML 
    public void shellSort(ActionEvent event){
       
         Shell x;
        x = new Shell();
        x.setTitle("Shell sort");
        x.setSize(500, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        
    }
    @FXML 
    
    public void radixSort(ActionEvent event){
       
         Radix x;
        x = new Radix();
        x.setTitle("Radix sort");
        x.setSize(500, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
}
