package com.mycompany.graphicssorting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author CinthyaLiliana
 */
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class Shell extends JFrame {
             int arr[] = new int[10];
    public Shell() {

        this.setSize(500, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        try {
            final DefaultCategoryDataset dataset = createDataset();
        } catch (InterruptedException ex) {
            Logger.getLogger(BubbleSort.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private DefaultCategoryDataset createDataset() throws InterruptedException {

        //final XYSeries series1 = new XYSeries("First");
       DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < 10; i++) {

            arr[i] = (int) (Math.random() * 999);
        }
       int inner, outer;
 
        int temp;
 
        int h = 1;  // find initial value of h 
 
        while (h <= arr.length / 3) {
            h = h * 3 + 1;  // (1, 4, 13, 40, 121, ...) 
        }
        while (h > 0) { // decreasing h, until h=1 
 
            for (outer = h; outer < arr.length; outer++) {
                temp = arr[outer];
 
                inner = outer;   // one subpass (eg 0, 4, 8) 
 
                while (inner > h - 1 && arr[inner - h] >= temp) {
                    arr[inner] = arr[inner - h];
 
                    inner -= h;
 
                }
                arr[inner] = temp;
                 for (int n = 0; n < arr.length; n++) {
                        dataset.addValue(arr[n],"Posición", ""+(n + 1));
                    }
                    
                    final JFreeChart chart = createChart(dataset);
                    final ChartPanel chartPanel = new ChartPanel(chart);
                    chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
                    setContentPane(chartPanel);
                    this.setSize(this.getWidth() +1, 400);
                    sleep(500);
                    
 
            } // end for 
 
            h = (h - 1) / 3;
 
        } // end while(h>0)
 
        
        return dataset;

    }
    

    private JFreeChart createChart(final DefaultCategoryDataset dataset) {

        // create the chart...
        JFreeChart barChart = ChartFactory.createBarChart(
                "SHELL SORT",
                "Position",
                "Values",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        
        
        return barChart;

    }
    
    
}
