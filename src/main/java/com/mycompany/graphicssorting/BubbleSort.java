package com.mycompany.graphicssorting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;



public class BubbleSort extends JFrame {

    public BubbleSort() {

        this.setSize(500, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        try {
            final DefaultCategoryDataset dataset = createDataset();
        } catch (InterruptedException ex) {
            Logger.getLogger(BubbleSort.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private DefaultCategoryDataset createDataset() throws InterruptedException {

        //final XYSeries series1 = new XYSeries("First");
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

         int arr[] = new int[10];

        for (int i = 0; i < 10; i++) {

            arr[i] = (int) (Math.random() * 999);
        }
        
        boolean swapped = true;
        int j = 0;
        int tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < arr.length - j; i++) {
                if (arr[i] > arr[i + 1]) {
                    tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    swapped = true;

                    for (int n = 0; n < arr.length; n++) {
                        dataset.addValue(arr[n],"Posición", ""+(n + 1));
                    }
                    
                    final JFreeChart chart = createChart(dataset);
                    final ChartPanel chartPanel = new ChartPanel(chart);
                    chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
                    setContentPane(chartPanel);
                    this.setSize(this.getWidth() +1, 400);
                    sleep(500);
                    //dataset.clear();
                }
            }
        }

        return dataset;

    }

    private JFreeChart createChart(final DefaultCategoryDataset dataset) {

        // create the chart...
        JFreeChart barChart = ChartFactory.createBarChart(
                "BUBBLE SORT",
                "Position",
                "Values",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        
        
        return barChart;

    }

}
