package com.mycompany.graphicssorting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author CinthyaLiliana
 */
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

public class Merge extends JFrame {
             int arr[] = new int[10];
    public Merge() {

        this.setSize(500, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        try {
            final DefaultCategoryDataset dataset = createDataset();
        } catch (InterruptedException ex) {
            Logger.getLogger(BubbleSort.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private DefaultCategoryDataset createDataset() throws InterruptedException {

        //final XYSeries series1 = new XYSeries("First");
       DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < 10; i++) {

            arr[i] = (int) (Math.random() * 999);
        }
        
         int out, in, min;
        int comparar=0, iterar=0;
        for (out = 0; out < arr.length - 1; out++) // outer loop
            
        {
            iterar ++;
            
            min = out; // minimum
            for (in = out + 1; in < arr.length; in++) // inner loop
            {
                comparar ++;
                if (arr[in] < arr[min]) // if min greater,
                {
                    min = in; // we have a new min
                }
            }
            dataset = swap(out, min, dataset); // swap them
            
        } // end for(out)
        
        return dataset;

    }
    
    private DefaultCategoryDataset swap(int one, int two, DefaultCategoryDataset dataset) throws InterruptedException {
        int temp = arr[one];
        arr[one] = arr[two];
        arr[two] = temp;
         for (int n = 0; n < arr.length; n++) {
                        dataset.addValue(arr[n],"Posición", ""+(n + 1));
                    }
                    
                    final JFreeChart chart = createChart(dataset);
                    final ChartPanel chartPanel = new ChartPanel(chart);
                    chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
                    setContentPane(chartPanel);
                    this.setSize(this.getWidth() +1, 400);
                    sleep(500);
                    
                    return dataset;
    }

    private JFreeChart createChart(final DefaultCategoryDataset dataset) {

        // create the chart...
        JFreeChart barChart = ChartFactory.createBarChart(
                "MERGE SORT",
                "Position",
                "Values",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        
        
        return barChart;

    }
}
