package com.mycompany.graphicssorting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author CinthyaLiliana
 */
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

public class Radix extends JFrame {
             int arr[] = new int[10];
    public Radix() {

        this.setSize(500, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        try {
            final DefaultCategoryDataset dataset = createDataset();
        } catch (InterruptedException ex) {
            Logger.getLogger(BubbleSort.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private DefaultCategoryDataset createDataset() throws InterruptedException {

        //final XYSeries series1 = new XYSeries("First");
       DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < 10; i++) {

            arr[i] = (int) (Math.random() * 999);
        }
        
        int i, m = arr[0], exp = 1, n = arr.length;
        int[] b = new int[10];
        for (i = 1; i < n; i++) {
            if (arr[i] > m) {
                m = arr[i];
            }
        }
        while (m / exp > 0) {
            int[] bucket = new int[10];
 
            for (i = 0; i < n; i++) {
                bucket[(arr[i] / exp) % 10]++;
            }
            for (i = 1; i < 10; i++) {
                bucket[i] += bucket[i - 1];
            }
            for (i = n - 1; i >= 0; i--) {
                b[--bucket[(arr[i] / exp) % 10]] = arr[i];
            }
            for (i = 0; i < n; i++) {
                arr[i] = b[i];
            }
            exp *= 10;
              for (int j = 0; j < arr.length; j++) {
                        dataset.addValue(arr[j],"Posición", ""+(j + 1));
                    }
                    
                    final JFreeChart chart = createChart(dataset);
                    final ChartPanel chartPanel = new ChartPanel(chart);
                    chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
                    setContentPane(chartPanel);
                    this.setSize(this.getWidth() +1, 400);
                    sleep(500);
            
        }
 
        return dataset;

    }
   

    private JFreeChart createChart(final DefaultCategoryDataset dataset) {

        // create the chart...
        JFreeChart barChart = ChartFactory.createBarChart(
                "RADIX SORT",
                "Position",
                "Values",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        
        
        return barChart;

    }
    
    
}
