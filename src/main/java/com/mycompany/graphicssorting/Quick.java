package com.mycompany.graphicssorting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author CinthyaLiliana
 */
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Quick extends JFrame {
             int arr[] = new int[10];
    public Quick() {

        this.setSize(500, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        try {
            final DefaultCategoryDataset dataset = createDataset();
        } catch (InterruptedException ex) {
            Logger.getLogger(BubbleSort.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private DefaultCategoryDataset createDataset() throws InterruptedException {

        //final XYSeries series1 = new XYSeries("First");
       DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < 10; i++) {

            arr[i] = (int) (Math.random() * 999);
        }
        
         int in, out;
        int comparar=0, iterar=0;
        for (out = 1; out < arr.length; out++) // out is dividing line
        {
            iterar++;
            int temp = arr[out]; // remove marked item
            in = out; // start shifts at out
            while (in > 0 && arr[in - 1] >= temp) // until one is smaller,
            {
                comparar++;
                arr[in] = arr[in - 1]; // shift item right,
                --in; // go left one position
            }
            arr[in] = temp; // insert marked item
             for (int n = 0; n < arr.length; n++) {
                        dataset.addValue(arr[n],"Posición", ""+(n + 1));
                    }
                    
                    final JFreeChart chart = createChart(dataset);
                    final ChartPanel chartPanel = new ChartPanel(chart);
                    chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
                    setContentPane(chartPanel);
                    this.setSize(this.getWidth() +1, 400);
                    sleep(500);
        } // end for
        
        return dataset;

    }
   

    private JFreeChart createChart(final DefaultCategoryDataset dataset) {

        // create the chart...
        JFreeChart barChart = ChartFactory.createBarChart(
                "QUICK SORT",
                "Position",
                "Values",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        
        
        return barChart;

    }
    
}